import math
import matplotlib.pyplot as plt

class PopulationModel:
    def __init__(self, br, dr, ip):
        self.birthRate = br
        self.deathRate = dr
        self.initPop = ip
        self.data = []

    def diffEqn(self, t):
        t0 = 0
        p_t = self.initPop*math.exp((self.birthRate - self.deathRate)*(t-t0))
        return p_t

    def months(self, n):
        for m in range(1, n):
            self.data.append(self.diffEqn(m))
        return self.data


def graph(data, i):
    f = plt.figure(i)
    plt.plot(data)
        



if __name__ == "__main__":
    fox = PopulationModel(.05, .0001, 500)
    rabbit = PopulationModel(.1, .009, 1000)

    graph( fox.months(100), 1 )
    graph( rabbit.months(100), 2 )

    plt.show()
    
