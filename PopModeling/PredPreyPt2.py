import matplotlib.pyplot as plt
import numpy as np

class LotkeVolterra:
    def __init__(self, B_r, Prey_rf, Prey_rt, p, q,
                       D_f, Pred_rf, Prey_ft, r, s,
                       D_t, Pred_rt, Pred_ft):
        self.B_r = B_r
        self.Prey_rf = Prey_rf
        self.Prey_rt = Prey_rt
        self.p = p
        self.q = q
        self.D_f = D_f
        self.Pred_rf = Pred_rf
        self.Prey_ft = Prey_ft
        self.r = r
        self.s = s
        self.D_t = D_t
        self.Pred_rt = Pred_rt
        self.Pred_ft = Pred_ft
        return

    def dx_dt(self, x, y, z):
        B_r = self.B_r
        Prey_rf = self.Prey_rf
        Prey_rt = self.Prey_rt
        p = self.p
        q = self.q
        return (B_r - Prey_rf*y - Prey_rt*z)*x + (p-q)*x*x

    def dy_dt(self, x, y, z):
        D_f = self.D_f
        Pred_rf = self.Pred_rf
        Prey_ft = self.Prey_ft
        r = self.r
        s = self.s
        return (Pred_rf*x - Prey_ft*z - D_f)*y + (r-s)*y*y

    def dz_dt(self, x, y, z):
        Pred_rt = self.Pred_rt
        Pred_ft = self.Pred_ft
        D_t = self.D_t
        return (Pred_rt*x + Pred_ft*y - D_t)*z      

if __name__ == "__main__":
    LV = LotkeVolterra( 0.09, .0004, .0005, 5e-10, 4e-10,
                        0.05, .0009, .0003, 4e-9,  3e-9,
                        0.06, .0004, .0003              )

    

    x = 100
    y = 100
    z = 100
    X = np.empty((0))
    Y = np.empty((0))
    Z = np.empty((0))
    for t in range(250):
        print(x, y, z)
        X = np.append(X, x)
        Y = np.append(Y, y)
        Z = np.append(Z, z)

        x_temp = x
        y_temp = y
        z_temp = z
        
        x += LV.dx_dt(x_temp, y_temp, z_temp)
        y += LV.dy_dt(x_temp, y_temp, z_temp)
        z += LV.dz_dt(x_temp, y_temp, z_temp)

        if x < 0:
            x = 0
        if y < 0:
            y = 0
        if z < 0:
            z = 0
        
    plt.plot(X)
    plt.plot(Y)
    plt.plot(Z)
    plt.show()

    
        
