class DiningProb():
    def __init__(self, a, b):
        self.aprob = a
        self.bprob = b


    def prob(self, n):
        a = 1
        b = 0
        for i in range(n):
             newa = a * self.aprob + b * (1-self.bprob)
             newb = a * (1-self.aprob) + b * self.bprob

             a = newa
             b = newb

        print(a, b)


if __name__ == "__main__":
    dp = DiningProb(0.25, 0.93)

    dp.prob(10)

    
