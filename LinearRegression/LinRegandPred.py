import csv
import math
from scipy import stats

def s(x, y, mode):
    summ = 0
    m = min(len(x), len(y))
    
    if mode == 'x':
        for i in range(m):
            summ += x[i]
    elif mode == 'y':
        for i in range(m):
            summ += y[i]
    elif mode == 'xy':
        for i in range(m):
            prod = x[i] * y[i]
            summ += prod
    elif mode == 'x^2':
        for i in range(m):
            sqrd = x[i] * x[i]
            summ += sqrd
    
    return summ

def mean(d):
    return s(d, d, 'x') / len(d)

def sumI(r, M):
    summ = 0
    for i in range(M):
        summ += r[i]
    return summ
    

def idiosyncraticRisk(r):
    return sumI(r, 100) + 2*sumI(r, 99)


def findSlope(x, y):
    m = min(len(x), len(y))
    return ( m*s(x, y, 'xy') - s(x, y, 'x')*s(x, y, 'y') )  \
             / ( m*s(x, y, 'x^2') - s(x, y, 'x')*s(x, y, 'x') )

def findIntercept(x, y):
    m = min(len(x), len(y))
    return ( s(x, y, 'x^2')*s(x, y, 'y') - s(x, y, 'xy')*s(x, y, 'x') )  \
             / ( m*s(x, y, 'x^2') - s(x, y, 'x') * s(x, y, 'x') )


if __name__ == "__main__":
    DJIA = csv.reader(open('DJIA.csv'))
    INTC = csv.reader(open('INTC.csv'))
    UNH = csv.reader(open('UNH.csv'))
    NKE = csv.reader(open('NKE.csv'))

    DowJonesData = []
    IntelData = []
    UnitedData = []
    NikeData = []
    
    for row in DJIA:
        DowJonesData.append(float(row[0]))

    for row in INTC:
        IntelData.append(float(row[0]))

    for row in UNH:
        UnitedData.append(float(row[0]))

    for row in NKE:
        NikeData.append(float(row[0]))

    for companyData in [IntelData, UnitedData, NikeData]:
        a = findSlope(DowJonesData, companyData)
        b = findIntercept(DowJonesData, companyData)
        ui = float(input())
        print("When DJIA is ", ui, "you can expect this stock to be at ", a*ui + b)
        print("This data has a correlation value of ",
              stats.pearsonr(DowJonesData, companyData)[0])
        print(100*idiosyncraticRisk(companyData)/s(companyData, companyData, 'xy'),
              '% idiosynchratic risk.')
        print()
        
