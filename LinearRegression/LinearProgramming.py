#Authors: Mitch Rosenlof and Brennen Robin
#Course:  CST-305
#Date:    October 13, 2018
#Description: In this project, we will analyse the fuel costs of a plane
#that is traveling across the country with four stops along the way. The goal
#is to minimize the cost of fuel, as for each 1000 gallons above the minumum
#fuel requirement, 50 gallons of excess will be burned. The question is, what
#is the optimal amount of fuel to have on the plane when leaving the ground
#at each leg of the journey?
import matplotlib.pyplot as plt
def main():
    fuel = 26000
    cost = AtlantaToLA(fuel)
    cost1 = .05*fuel + 10800
    print(cost, cost1)
    fuel -= cost
    print(fuel)
    cost = LAToHouston(fuel)
    fuel -= cost
    print(fuel)
    cost = HoustonToNO(fuel)
    fuel -= cost
    print(fuel)
    cost = NOToAtlanta(fuel)
    fuel -= cost
    print(fuel)

   # plotLine()

def AtlantaToLA(x):
    return 12000 + .05*(x-24000)

def LAToHouston(x):
    return 7000 + .05*(x-15000)

def HoustonToNO(x):
    return 3000 + .05*(x-9000)

def NOToAtlanta(x):
    return 5000 + .05*(x-20000)

def plotLine():
    points = []
    for i in range(24000,36000):
        points.append(AtlantaToLA(i))
    print(points)
    plt.plot(points)
    plt.show()


if __name__ == "__main__":
    main()
    
