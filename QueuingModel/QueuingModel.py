import random
import math
import matplotlib.pyplot as plt
from matplotlib import animation

MAX_POS = 20
TOTAL_TIME = 480 #8 hours

class customer:
    def __init__(self):
        self.numItems = random.randint(1, 100)
        self.checkoutTime = math.floor( self.numItems * .1 )

    def getNumItems(self):
        return self.numItems

    def getCheckoutTime(self):
        return self.checkoutTime

    def increaseCheckoutTime(self, t):
        self.checkoutTime += t
        
    def decreaseCheckoutTime(self, t):
        self.checkoutTime -= t

class checkoutLine:
    def __init__(self):
        self.q = []
        self.waitTime = 0
        self.workers = 0
        self.open = False

    def isOpen(self):
        return self.open

    def getQ(self):
        return self.q

    def push(self, elem):
        self.q.append(elem)

    def nextInLine(self):
        return self.q[0]

    def pop(self):
        del self.q[0]

    def stealCustomerFromBack(self):
        c = self.q[-1]
        del self.q[-1]
        return c

    def increaseWaitTime(self, t):
        self.waitTime += t

    def decreaseWaitTime(self, t):
        self.waitTime -= t

    def getLength(self):
        return len(self.q)

    def getWaitTime(self):
        return self.waitTime

    def openLine(self):
        self.open = True
        self.workers += 1

    def addBagger(self):
        self.workers += 1

    def closeLine(self):
        self.open = False
        self.workers = 0

    def getNumWorkers(self):
        return self.workers

class Store:
    def __init__(self, numPOS):
        self.openI = numPOS #index to open a POS
        self.pool = 20 - numPOS
        self.probForgotWallet = 0.2
        self.probForgotItems = 0.3
        self.customers = []
        self.checkouts = []
        for i in range(numPOS):
            newLine = checkoutLine()
            newLine.openLine()
            if self.pool >= 1:
                newLine.addBagger()
                self.pool -= 1
            self.checkouts.append(newLine)
        for i in range(MAX_POS-numPOS):
            newLine = checkoutLine()
            self.checkouts.append(newLine)

    def getOpenI(self):
        return self.openI

    def addNewCustomers(self, c):
        self.customers.extend(c)

    def shoppingCustomers(self):
        return self.customers

    def numShoppingCustomers(self):
        return len(self.customers)

    def doneShopping(self, n):
        done = self.customers[:n]
        del self.customers[:n]
        return done

    def findShortestWait(self):
        shortest = self.checkouts[0]
        for c in self.checkouts[:self.openI]:
            if c.getWaitTime() < shortest.getWaitTime() and c.getLength() < 50:
                shortest = c
        return shortest

    def funnelCustomers(self, customers):
        for customer in customers:
            shortest = self.findShortestWait()
            shortest.push(customer)
            shortest.increaseWaitTime( customer.getCheckoutTime() )

    def checkoutCustomers(self):
        for line in self.checkouts[:self.openI]:
            if line.getLength() != 0:
                customer = line.nextInLine()
                r = random.uniform(0, 1)
                if r < self.probForgotWallet:
                    line.pop()
                if r < self.probForgotItems:
                    numItems = random.randint(1, 100)
                    customer.increaseCheckoutTime(math.floor( numItems * .1 ))
                    continue
                    
                if customer.getCheckoutTime() <= 1:
                    line.pop()
                else:
                    n = line.getNumWorkers()
                    customer.decreaseCheckoutTime(n)
                    line.decreaseWaitTime(n)
            

    def getCheckouts(self):
        return self.checkouts


    def linesTooLong(self):
        count = 0
        for i in range(self.openI):
            if self.checkouts[i].getLength() >= 30:
                count += 1

        if count/len(self.checkouts[:self.openI]) >= 0.4:
            return True
        else:
            return False

    def linesTooShort(self):
        count = 0
        for i in range(self.openI):
            if self.checkouts[i].getLength() <= 15:
                count += 1

        if count/len(self.checkouts[:self.openI]) >= 0.6:
            return True
        else:
            return False
            

    def snagFromLongestLine(self):
        longest = self.checkouts[0]
        for c in self.checkouts:
            if c.getLength() > 0 and c.getWaitTime() > longest.getWaitTime():
                longest = c

        customer = longest.stealCustomerFromBack()
        return customer

    def closePOS(self):
        self.openI -= 1
        closing = self.checkouts[self.openI]
        self.checkouts[self.openI].closeLine()
        for customer in closing.getQ():
            shortest = self.findShortestWait()
            shortest.push(customer)
            shortest.increaseWaitTime(customer.getCheckoutTime())
        del closing.getQ()[:]


        
    def openPOS(self):
        self.checkouts[self.openI].openLine()
        if self.pool >= 1:
            self.checkouts[self.openI].addBagger()
            self.pool -= 1

        lengthsSum = 0
        for c in self.checkouts[:self.openI]:
            lengthsSum += c.getLength()
        avg = lengthsSum / len(self.checkouts[:self.openI])
        r = random.randint(0, self.openI)
        for i in range(math.floor(avg/20)):
            customer = self.snagFromLongestLine()
            line = self.checkouts[self.openI]
            line.push(customer)
            line.increaseWaitTime(customer.getCheckoutTime())

        self.openI += 1

def generateCustomers(t):
    r = 2
    if t <= TOTAL_TIME*0.2 or t >= TOTAL_TIME*0.8:
        r = random.randint(2, 4)
    elif t <= TOTAL_TIME*0.4 or t >= TOTAL_TIME*0.6:
        r = random.randint(5, 7)
    else:
        r = random.randint(8, 10)
    return [customer() for i in range(r)]

def linesOccupied(S):
    for c in S.getCheckouts():
        if c.getLength() > 0:
            return True
    return False

def openForBusiness(S):
    rects = initGraph()
    minute = 0

    while True:
        displayGraph(rects, S)
    
        if minute < TOTAL_TIME:
            newCustomers = generateCustomers(minute)
            S.addNewCustomers(newCustomers)

        if len(S.shoppingCustomers()) >= 1:   
            n = random.randint(1, len(S.shoppingCustomers()))
            done = S.doneShopping(n)
            S.funnelCustomers(done)
            
        S.checkoutCustomers()

        if S.linesTooLong() and S.getOpenI() < 20:
            S.openPOS()

        if minute >= 100 and S.linesTooShort() and S.getOpenI() > 1:
            S.closePOS()
            
        if not linesOccupied(S): break

        minute += 1
    return

def initGraph():
    plt.xticks([i for i in range(MAX_POS)], [i+1 for i in range(MAX_POS)])
    plt.ylim(0,50)
    rects = plt.bar([i for i in range(MAX_POS)],
                    [0 for i in range(MAX_POS)],
                    color=[0, 0, 0,1])
    return rects
    
def displayGraph(rects, S):
    qLengths = []
    for c in S.getCheckouts():
        qLengths.append(c.getLength())
    for rect, h in zip(rects, qLengths):
        rect.set_height(h)

    txt = plt.figtext(0.6, 0.9, "Customers shopping: " + str(S.numShoppingCustomers()))
    plt.pause(0.0001)
    txt.remove()   

    
if __name__ == "__main__":
    SuperMarket = Store(1)

    openForBusiness(SuperMarket)
