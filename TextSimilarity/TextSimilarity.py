def fileToStr(fn):
    s = ""
    with open(fn) as f:
        for line in f:
            s += line[:-1]
            s += ' '
    return s

def lengthSim(s1, s2):
    if len(s1) > len(s2):
        sim = 0.3 * len(s2)/len(s1)
    else:
        sim = 0.3 * len(s1)/len(s2)
    return sim

def wordcountSim(l1, l2):
    wcounts1 = {}
    wcounts2 = {}
    for i in range(len(min(l1, l2))):
        if not l1[i] in wcounts1:
            wcounts1[l1[i]] = 1
        else:
            wcounts1[l1[i]] += 1

        if not l2[i] in wcounts2:
            wcounts2[l2[i]] = 1
        else:
            wcounts2[l2[i]] += 1

    count = 0
    for i in range(len(min(l1, l2))):
        word = l1[i]
        if word in wcounts1 and word in wcounts2:
            if abs(wcounts1[word] - wcounts2[word]) < 3:
                count += 1
    return 0.4 * count / len(min(l1, l2))

def indexSim(l1, l2):
    count = 0
    for i in range(len(min(l1, l2))):
        if l1[i] == l2[i]:
            count += 1
    return 0.3 * count / len(min(l1, l2))
    

if __name__ == "__main__":
    s1 = fileToStr("s1.txt")
    s2 = fileToStr("s2.txt")
    l1 = s1.split(' ')
    l2 = s2.split(' ')

    ls = lengthSim(s1, s2)
    wcs = wordcountSim(l1, l2)
    inds = indexSim(l1, l2)
    if ls + wcs + inds >= 0.7:
        print("s1 and s2 are similar")
    else:
        print("s1 and s2 are not similar")

    

    
